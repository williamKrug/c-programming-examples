/* Author: William Krug
   Class: CSCI 1111-90
   Assignment: Exercise #25
   Purpose: Use qsort() to sort an array of your structures.
   FILE: exercise25_qsort().c */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 5

struct vehicle
{
	char description[20];
	int year;
	char make[30];
	char model[30];
	float displacement;
};

void print_vehicle(struct vehicle name);
int description_compare(void const * p1, void const *p2);
int year_compare(void const * p1, void const *p2);

int main()
{
	int i;
	struct vehicle inventory[SIZE];
	
	strcpy(inventory[0].description, "exotic");
	inventory[0].year = 2017;
	strcpy(inventory[0].make, "Bugatti");
	strcpy(inventory[0].model, "Veyron");
	inventory[0].displacement = 16.4;
	
	strcpy(inventory[1].description, "domestic");
	inventory[1].year = 2007;
	strcpy(inventory[1].make, "Ford");
	strcpy(inventory[1].model, "Edge");
	inventory[1].displacement = 3.5;
	
	strcpy(inventory[2].description, "foreign");
	inventory[2].year = 2012;
	strcpy(inventory[2].make, "Audi");
	strcpy(inventory[2].model, "R8");
	inventory[2].displacement = 5.2;
	
	strcpy(inventory[3].description, "muscle");
	inventory[3].year = 1967;
	strcpy(inventory[3].make, "Ford");
	strcpy(inventory[3].model, "Shelby GT500");
	inventory[3].displacement = 4.7;
	
	strcpy(inventory[4].description, "dream");
	inventory[4].year = 1967;
	strcpy(inventory[4].make, "Pontiac");
	strcpy(inventory[4].model, "GTO");
	inventory[4].displacement = 6.5;
	
	printf("\tUnsorted array:\n");
	printf("\t---------------\n");
	for(i = 0; i < SIZE; i++)
		print_vehicle(inventory[i]);
	printf("\n");
		
	printf("\tSorted by Description:\n");
	printf("\t----------------------\n");
	qsort(inventory, SIZE, sizeof(struct vehicle), description_compare);
	for(i = 0; i < SIZE; i++)
		print_vehicle(inventory[i]);
	printf("\n");
		
	printf("\tSorted by Model Year:\n");
	printf("\t---------------------\n");
	qsort(inventory, SIZE, sizeof(struct vehicle), year_compare);
	for(i = 0; i < SIZE; i++)
		print_vehicle(inventory[i]);
	
	return 0;
}

void print_vehicle(struct vehicle name)
{
	printf("\t%s\n", name.description);
	printf("\t  Year: %d\n", name.year);
	printf("\t  Make: %s\n", name.make);
	printf("\t Model: %s\n", name.model);
	printf("\tEngine: %.1fL\n", name.displacement);
	printf("\n");

}

int description_compare(void const * p1, void const *p2)
{
	return strcmp(((struct vehicle*)p1)->description, ((struct vehicle*)p2)->description);
}

int year_compare(void const * p1, void const *p2)
{
	return ((struct vehicle*)p1)->year - ((struct vehicle*)p2)->year;
}