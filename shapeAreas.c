/*	Author: William Krug
	Class: CSCI 1111-90
	Assignement: Area of various shapes
	FILE: geometryArea.c	*/


#include <stdio.h>
#include <math.h>

void print_menu(void);
float areaCircle(void);
float areaRectangle(void);
float areaSquare(void);
float areaTriangle(void);

int main( )
{
  int ch;

  /* Display menu of choices */
  print_menu( );
  ch =getchar( );

  /* Loop to quit on upper or lower case Q */
  while(ch != 'q' && ch != 'Q'){
    switch(ch)
	{
      case 'c':
      case 'C':
      	 printf("\n");
         printf("\n\tThe area of your Circle is %.2f units sq.", areaCircle());
         break;
      case 'r':
      case 'R':
      	 printf("\n");
         printf("\tThe area of your Rectangle is %.2f units sq.", areaRectangle());
         break;
      case 's':
      case 'S':
      	 printf("\n");
         printf("\tThe area of your Square is %.2f units sq.", areaSquare());
         break;
      case 't':
      case 'T':
      	 printf("\n");
         printf("\tThe area of your Triangle is %.2f units sq.", areaTriangle());
         break;
      default:
      	 printf("\n");
         printf("\tInvalid choice- '%c'.\n", ch);
         break;
    }

    getchar( );         /* strip trailing newline */

    /* Display menu of choices */
    printf("\n\n");
    print_menu( );

    ch =getchar( );
  }

  return 0;
}

void print_menu(void)
{
  printf("\tC- Circle\n");
  printf("\tR- Rectangle\n");
  printf("\tS- Square\n");
  printf("\tT- Triangle\n");

  printf("\n\tQ- to quit\n");
  printf("\n\n\tChoice: ");

  return;
}

float areaCircle(void)
{
	float radius;
	float areaC;
	
	printf("\tWhat is the radius of your circle: ");
	scanf(" %f", &radius);
	
	areaC = M_PI * radius * radius;
	
	return areaC;
}

float areaRectangle(void)
{
	float length;
	float width;
	char oops;
	float areaR;
	
	printf("\tWhat is the length of your rectangle: ");
	scanf(" %f", &length);
	
	printf("\tWhat is the width of your rectangle: ");
	scanf(" %f", &width);
	
	if(length == width)
	{
		printf("\tYou entered the dimensions for a square.\n");
		printf("\tAre you sure these are the correct dimensions? y/n: ");
		scanf(" %c", &oops);

		printf("\n\n");
		if(oops == 'y' || oops == 'Y')
		{
			printf("\tYou are right in thinking a square can be a rectangle.");
		}
		else
		{
			printf("\tWhat is the length of your rectangle: ");
			scanf(" %f", &length);
	
			printf("\tWhat is the width of your rectangle: ");
			scanf(" %f", &width);
		}	
	}
	
	areaR = length * width;
	
	return areaR;
}

float areaSquare(void)
{
	float length;
	float width;
	float areaS;
	char oops;
	
	printf("\tAre the length and width the same dimension? y/n: ");
	scanf(" %c", &oops);
	
	printf("\n\n");
	if(oops == 'y' || oops == 'Y')
	{
		printf("\tWhat is the length of one side of the square: ");
		scanf(" %f", & length);
		
		areaS = length * length;
	}
	else
	{
		printf("\tYou are dealing with a rectangle, not a square.\n");
		
		printf("\tWhat is the length of your rectangle: ");
		scanf(" %f", &length);
	
		printf("\tWhat is the width of your rectangle: ");
		scanf(" %f", &width);
		
		areaS = length * width;
	}
	
	return areaS;
	
}

float areaTriangle(void)
{
	float length;
	float height;
	float areaT;
	
	printf("\tWhat is the length of your triangle: ");
	scanf(" %f", &length);
	
	printf("\tWhat is the height of your triangle: ");
	scanf(" %f", &height);
	
	areaT = (length * height) / 2;
	
	return areaT;
}