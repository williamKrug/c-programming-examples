/* Author: William Krug
   Class: CSCI 1111-90
   Assignement: Exercise #23
   Purpose: Create a struct of your own that uses several different
   			data types for members. {At least 3.}
   			Write a display function that displays the contents of
   			your struct type, pass the struct as a parameter to the
   			display function.
   FILE: exercise23_struct.c */
   
#include <stdio.h>
#include <string.h>

#define LITER 'L'

struct vehicle
{
	char name[20];
	int year;
	char make[30];
	char model[30];
	float displacement;
};

void print_vehicle(struct vehicle name);

int main()
{
	struct vehicle exotic;
	struct vehicle domestic;
	struct vehicle foreign;
	struct vehicle muscle;
	struct vehicle dream;
	
	strcpy(exotic.name, "exotic");
	exotic.year = 2017;
	strcpy(exotic.make, "Bugatti");
	strcpy(exotic.model, "Veyron");
	exotic.displacement = 16.4;
	
	strcpy(domestic.name, "domestic");
	domestic.year = 2007;
	strcpy(domestic.make, "Ford");
	strcpy(domestic.model, "Edge");
	domestic.displacement = 3.5;
	
	strcpy(foreign.name, "foreign");
	foreign.year = 1012;
	strcpy(foreign.make, "Audi");
	strcpy(foreign.model, "R8");
	foreign.displacement = 5.2;
	
	strcpy(muscle.name, "muscle");
	muscle.year = 1967;
	strcpy(muscle.make, "Ford");
	strcpy(muscle.model, "Shelby GT500");
	muscle.displacement = 4.7;
	
	strcpy(dream.name, "dream");
	dream.year = 1967;
	strcpy(dream.make, "Pontiac");
	strcpy(dream.model, "GTO");
	dream.displacement = 6.5;
	
	print_vehicle(exotic);
	print_vehicle(domestic);
	print_vehicle(foreign);
	print_vehicle(muscle);
	print_vehicle(dream);
	
	return 0;
}

void print_vehicle(struct vehicle name)
{
	printf("\t%s\n", name.name);
	printf("\t  Year: %d\n", name.year);
	printf("\t  Make: %s\n", name.make);
	printf("\t Model: %s\n", name.model);
	printf("\tEngine: %.1f%c\n", name.displacement, LITER);
	printf("\n\n");
	
	return;
}