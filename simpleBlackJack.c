/* Author: William Krug
   Class: CSCI 1111-90
   Assignment: Exercise #11
   Purpose: Write a function cardValue() that is passed a single
            integer parameter representing a playing card (0-51)
            and returns the cards value in the game of BlackJack.
    FILE: exercise11_cardValue.c    */
    
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char welcome();
int cardValue(int card);

int main()
{
    int card;
	int value = 0;
    int handTotal = 0;
    char response;
    time_t seed;
    
    seed = time(NULL);
    srand(seed);
    
    response = welcome();
	
    if(response == 'y')
    {
    	while(response == 'y')
    	{
			card = rand() % 52;
    		
			value = cardValue(card);
    		
    		handTotal = handTotal + value;
    		
    		if(handTotal <= 21)
    		{
    			printf("\n\tThe total points of your hand is %d.\n", handTotal);
				printf("\tWould you like another card? y/n ");
				scanf(" %c", &response);
    			
    			if(response == 'n')
    			{
    				break;
				}
			}
			else
			{
			    response = 'n';
            }
		}
		if(handTotal <= 21)
		{
		    printf("\n\tYou are staying at %d.\n", handTotal);
        }
        else
        {
            printf("\a\a\n\tBust!");
            response = 'n';
        }
	}
	else
	{
		printf("\tGoodbye");
	}
	
	return 0;
}

char welcome()
{
	char yesOrNo;
	
	printf("\tWelcome to BlackJack!\n");
	printf("\tDo you want to be dealt in? ");
	scanf(" %c", &yesOrNo);
	
	return yesOrNo;	
}

int cardValue(int card)
{
	int i = 0;
    char suits[4][9] = {"Spades", "Hearts", "Clubs", "Diamonds"};
    
    if(card % 13 + 1 == 1)
	{
		printf("\n\tYou got the %s of %s.\n", "Ace", suits[card / 13]);
		i = 11;
	}
	else if((card % 13 + 1 >= 2) && (card % 13 + 1 <=10))
	{
		printf("\n\tYou got the %d of %s.\n", card % 13 + 1, suits[card / 13]);
		i = card % 13 + 1;
	}
	else if(card % 13 + 1 == 11)
	{
		printf("\n\tYou got the %s of %s.\n", "Jack", suits[card / 13]);
		i = 10;
	}
	else if(card % 13 + 1 == 12)
	{
		printf("\n\tYou got the %s of %s.\n", "Queen", suits[card / 13]);
		i = 10;
	}
	else if(card % 13 + 1 == 13)
	{
		printf("\n\tYou got the %s of %s.\n", "King", suits[card / 13]);
		i = 10;
	}
	
	return i;
}